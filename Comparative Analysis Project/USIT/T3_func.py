import numpy as np
import numba as nb
from numba import jit
import pint
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.cm import get_cmap
import copy
import dlish5 as dh5
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pandas as pd
import scipy as sp
import scipy.signal as sig
from scipy.optimize import least_squares
from tqdm import tqdm as tqdm
from tqdm.notebook import tqdm as tqdmnb

ureg = pint.UnitRegistry()



# hello

class PyFile:

    def __init__(self, dh5_file):
        self.file = dh5_file

    def prop(self):
        # Loading the HDF5 File which is a converted DLIS file of the well
        casing_thick = self.file.find_parameter_value('THNO').value  # unit in inch
        casing_thick_in = casing_thick * ureg.inch
        casing_thick_ft = casing_thick_in.to(ureg.ft)
        casing_thick_ft = casing_thick_ft.magnitude  # unit in foot

        casing_vel = 1 / self.file.find_parameter_value('VCAS').value  # unit in foot/us
        casing_vel_ft_us = casing_vel * ureg.ft / (ureg.us)
        casing_vel_m_s = casing_vel_ft_us.to(ureg.m / (ureg.s))
        casing_vel_m_s = casing_vel_m_s.magnitude

        time_reson = 2 * casing_thick_ft / casing_vel  # unit in us

        freq_reson = (1 / time_reson) * 1e6  # unit in Hz
        freq_range_min = freq_reson - (0.5 * freq_reson)
        freq_range_max = freq_reson + (0.5 * freq_reson)

        # Loading the USIT time parameter to define the time axis of the waveform
        USTO = self.file.find_parameter_value('USTO')  # Loading the time offset channel, unit in us
        USTO = np.array(USTO)

        NPPW = self.file.find_parameter_value('NPPW')  # Loading the parameter describring the number of samples per waveform
        NPPW = np.array(NPPW)
        samplenum_all = np.arange(0, NPPW)

        z_casing = self.file.find_parameter_value('ZCAS').value

        return casing_vel_m_s, time_reson, freq_range_min, freq_range_max, USTO, samplenum_all, z_casing


# Defining a function to convert from time to sample number, and vice versa
sampling_rate = 2000000  # USIT data sampling rate is 2 MHz
sampling_time_us = (1 / sampling_rate) * 1e6


def time_to_samplenum(WFDL_sel, time, USTO):
    return int(time / sampling_time_us) - WFDL_sel - USTO


def samplenum_to_time(WFDL_sel, samplenum, USTO):
    return WFDL_sel + USTO + (sampling_time_us * samplenum)


# Defining the frequency samples for the Fourier Transform
total_freq_sample = 512
freq = np.fft.rfftfreq(total_freq_sample, 1 / sampling_rate)  # 300 frquency samples are used
freq_mhz = freq / 1e6
freq_rads = 2 * np.pi * freq_mhz


def freq_range(freq_range_min, freq_range_max):
    start_selected_freq = (freq > freq_range_min).argmax()
    end_selected_freq = len(freq) - (freq[::-1] < freq_range_max).argmax()
    return start_selected_freq, end_selected_freq


# Processing the waveform
# Defining the functions to process the waveform

def firstecho(WFDL_sel, raw_wave_sel, USTO):
    raw_wave_env = np.abs(sig.hilbert(
        raw_wave_sel))  # Generating the envelope of each raw waveform
    samplenum_firstecho = np.argmax(
        raw_wave_env)  # Finding the sample number which corresponds to time of the first echo
    time_firstecho = samplenum_to_time(WFDL_sel,
                                       samplenum_firstecho, USTO)  # Finding the time of the first echo
    return raw_wave_env, samplenum_firstecho, time_firstecho


def win(firstecho_time, firstecho_samplenum, WFDL_sel, raw_wave_sel, win_halfwidth, time_reson, USTO, samplenum_all):
    samplenum_window_end = firstecho_samplenum + int((win_halfwidth * time_reson) / sampling_time_us)
    full_hamwin = np.hamming(((int((win_halfwidth * time_reson) / sampling_time_us)) * 2) - 1)
    idx_max_hamwin = len(full_hamwin)

    if (firstecho_samplenum + 1) <= (int((win_halfwidth * time_reson) / sampling_time_us)):
        idx_min_hamwin = int(((idx_max_hamwin + 1) / 2) - (firstecho_samplenum + 1))
        idx_min_raw_wave_win = 0
    else:
        idx_min_hamwin = 0
        idx_min_raw_wave_win = int(samplenum_window_end - idx_max_hamwin)

    hamwin = full_hamwin[idx_min_hamwin:]
    raw_wave_win = raw_wave_sel[idx_min_raw_wave_win:samplenum_window_end]
    timeaxis_all = samplenum_to_time(WFDL_sel, samplenum_all, USTO)
    timeaxis_win = timeaxis_all[idx_min_raw_wave_win:samplenum_window_end]
    windowed_sig = raw_wave_win * hamwin
    spec = np.fft.rfft(windowed_sig, total_freq_sample)

    return windowed_sig, hamwin, timeaxis_all, timeaxis_win, spec, idx_min_hamwin, idx_min_raw_wave_win, idx_max_hamwin, samplenum_window_end


def freqdom(spec):
    spec_mag = abs(spec)
    spec_mag_db = 20 * np.log10(spec_mag)
    spec_pha_rad = np.unwrap(np.angle(spec))
    groupdel = -np.gradient(spec_pha_rad, freq_rads)

    return spec_mag, spec_mag_db, spec_pha_rad, groupdel


def find_roots(x, y):
    s = np.abs(np.diff(np.sign(y))).astype(bool)
    return x[:-1][s] + np.diff(x)[s] / (np.abs(y[1:][s] / y[:-1][s]) + 1)


@jit(nopython=True, fastmath=True)
def polyfit(x, y, deg):
    mat = np.zeros(shape=(x.shape[0], deg + 1))
    mat[:, 0] = np.ones_like(x)
    for n in range(1, deg + 1):
        mat[:, n] = x ** n

    p = np.linalg.lstsq(mat, y)[0]
    return p


@jit(nopython=True, fastmath=True)
def polyder(p):
    d = np.zeros(shape=(p.shape[0] - 1))
    for n in range(d.shape[0]):
        d[n] = (n + 1) * p[n + 1]
    return d


@jit(nopython=True, fastmath=True)
def polyval(p, x):
    result = np.zeros_like(x)
    for coeff in p[::-1]:
        result = x * result + coeff
    return result


class NBPolynomial(
    np.polynomial.Polynomial):

    def __init__(self, coef, domain=None, window=None):
        # `domain` and `window` are ignored; kept for class compatibility
        coef = np.array(coef, dtype=float)
        np.polynomial.Polynomial.__init__(self, coef, domain, window)

    @staticmethod
    def fit(x, y, deg, **kwargs):
        # Other `kwargs` are ignored; kept for class compatibility
        return NBPolynomial(polyfit(x, y, deg))

    def __call__(self, x):
        return polyval(self.coef, x)


def groupdel_char(groupdel_subtracted, start_selected_freq, end_selected_freq, casing_vel_m_s):
    samplenum_f0 = np.argmin(groupdel_subtracted[start_selected_freq:end_selected_freq])
    samplenum_f0 = start_selected_freq + samplenum_f0

    polyfit_freq = freq_mhz[(samplenum_f0 - 1):(samplenum_f0 + 2)]
    polyfit_groupdel = groupdel_subtracted[(samplenum_f0 - 1):(samplenum_f0 + 2)]
    fitx = polyfit(polyfit_freq, polyfit_groupdel, 2)
    f0 = -fitx[1] / (2 * fitx[2])
    tx = np.linspace(f0, f0, 1)
    groupdel_min = polyval(fitx, tx)

    delta_tau = 0.4 * groupdel_min
    casing_thick_calc_m_init = ((1 / (f0 * 1e6)) * casing_vel_m_s) / 2  # Unit in m

    left_freq = freq_mhz[start_selected_freq:samplenum_f0]
    right_freq = freq_mhz[samplenum_f0:end_selected_freq]
    left_groupdel_subtracted = groupdel_subtracted[start_selected_freq:samplenum_f0]
    right_groupdel_subtracted = groupdel_subtracted[samplenum_f0:end_selected_freq]

    left_bw = find_roots(left_freq, (left_groupdel_subtracted - delta_tau))
    right_bw = find_roots(right_freq, (right_groupdel_subtracted - delta_tau))

    return groupdel_min, delta_tau, f0, left_bw, right_bw, casing_thick_calc_m_init


def findbw(groupdel_proc_subtracted_char_leftbw, groupdel_proc_subtracted_char_rightbw,
           groupdel_proc_subtracted_char_fzero):
    if len(groupdel_proc_subtracted_char_leftbw) >= 1 and len(groupdel_proc_subtracted_char_rightbw) >= 1:
        start_bw = groupdel_proc_subtracted_char_leftbw[-1]
        end_bw = groupdel_proc_subtracted_char_rightbw[0]
        bw = (
                     end_bw - start_bw) / groupdel_proc_subtracted_char_fzero  # Actually this is frac bandwidth in MHz
    else:
        start_bw = np.nan
        end_bw = np.nan
        bw = np.nan
    return start_bw, end_bw, bw


# Model response computation

def model_response(z_cement_init, casingthick_calc_m_init, specnorm, idxmin_rawwave_win, samplenum_procwindow_end,
                   hamwinproc, freqdomnorm_groupdel, z_mud, casing_vel_m_s, z_casing):
    k = (2 * np.pi * freq) / casing_vel_m_s
    ref_coeff = (((1 - (z_mud / z_cement_init)) * np.cos(k * casingthick_calc_m_init)) + (
            ((z_casing / z_cement_init) - (z_mud / z_casing)) * np.sin(k * casingthick_calc_m_init)) * 1j) / \
                (((1 + (z_mud / z_cement_init)) * np.cos(k * casingthick_calc_m_init)) + (
                        ((z_casing / z_cement_init) + (z_mud / z_casing)) * np.sin(
                    k * casingthick_calc_m_init)) * 1j)
    model_spec_freqdommul = ref_coeff * specnorm
    model_spec_freqdommul_timedom = np.fft.irfft(model_spec_freqdommul)
    model_spec_freqdommul_timedom_timedommul = model_spec_freqdommul_timedom[
                                               idxmin_rawwave_win:samplenum_procwindow_end] * hamwinproc
    model_spec = np.fft.rfft(model_spec_freqdommul_timedom_timedommul, total_freq_sample)

    model_spec_pha_rad = np.unwrap(np.angle(model_spec))
    model_groupdel = (-np.gradient(model_spec_pha_rad, freq_rads)) - freqdomnorm_groupdel

    return ref_coeff, model_spec_freqdommul_timedom, model_spec_freqdommul_timedom_timedommul, model_groupdel


def model_opt(val_opt, f0_meas, bw_meas, specnorm_opt, idxmin_rawwave_win_opt, samplenum_procwindow_end_opt,
              hamwinproc_opt, freqdomnorm_groupdel_opt, z_mud_sel, start_selected_freq, end_selected_freq, casing_vel_m_s, z_casing):
    _, _, _, groupdel_model_opt_model_groupdel = model_response(val_opt[0], val_opt[1], specnorm_opt,
                                                                idxmin_rawwave_win_opt, samplenum_procwindow_end_opt,
                                                                hamwinproc_opt, freqdomnorm_groupdel_opt, z_mud_sel, casing_vel_m_s, z_casing)
    _, _, groupdel_model_subtracted_char_opt_f0, groupdel_model_subtracted_char_opt_left_bw, groupdel_model_subtracted_char_opt_right_bw, _ = groupdel_char(
        groupdel_model_opt_model_groupdel, start_selected_freq, end_selected_freq, casing_vel_m_s)
    _, _, groupdel_model_subtracted_char_opt_modelbw = findbw(groupdel_model_subtracted_char_opt_left_bw,
                                                              groupdel_model_subtracted_char_opt_right_bw,
                                                              groupdel_model_subtracted_char_opt_f0)
    f0_model = groupdel_model_subtracted_char_opt_f0
    bw_model = groupdel_model_subtracted_char_opt_modelbw
    return ((f0_model - f0_meas) ** 2) + (((bw_model - bw_meas) * 2) ** 2)
